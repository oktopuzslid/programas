Ray Tracer
Ian Mallett

-----------

Lately, I've realized that while PyOpenGL is fast and powerful, it may not be quite powerful enough to create the realism I want easily.

Ray-tracing is a way to create super-realistic lighting effects, among other things. In the real world, light is emitted from light sources, bounces around in the world, then to your eye.  This is essentially what Ray-tracing does, only it works backwards, computing only the rays that hit the camera.  Each ray can be traced to the point where it stopped reflecting, so it essentially supports infinite reflections.  Shadows, refraction and more are also feasible.  All this is impossible with PyOpenGL.

-----------

Here's how you make your own custom scenes!  Currently, the system is not fully crash-proof, you you'll need to follow this pretty exactly.

The scene is loaded from Scene.txt.

I store old scenes in Scenes/.  The current scene must be named "Scene.txt" and be in the current directory.

As the header at the top announces, you may use # to signify comments.

Each line represents a data object.  Unsuprisingly, you specify your scene with these.

"SIZE" tells the size the image will render at.  

"BACK" tells how far the eye is from the image.  Normally, this shouldn't be too small.  Leaving it as-is is a good choice.

"BGCL" tells the background color.  For realism, I like to use (0,0,0) black, as there is no ambient lighting.

"LIGHT" defines a light at the given point.  There can be multiple lights, and adding them is as simple as adding another line.  There can be any number of lights, including 0.

"LIGHTING", "SHADOWS", "REFLECTION", and "REFRACTION" tell whether the relevant options should be used when rendering.  Currently, refraction is NOT working.  Keep it "False".

"scale" scales an object.

"trans" moves the origin to a new location to draw an object.  

if the line ends in ".obj" the line loads a .obj file of the same name.  See the .obj tutorial for instructions.

"rot" rotates the scene.  This can be a bit tricky to understand, so you may have to play with it to figure it out.  the number gives the amount to rotate, in degrees, and the letter tells which axis (X, Y, or Z).

"SPHERE" defines a sphere.  The tuple defines the sphere's position.  The number defines the sphere's radius.  The string at the end defines the material.  Valid materials are:
Metals:
--"Gold"
--"Silver"
Plastics (not available in .obj files):
--"Red Plastic"
--"Orange Plastic"
--"Yellow Plastic"
--"Green Plastic"
--"Blue Plastic"
--"Purple Plastic"
--"Black Plastic"
--"White Plastic"
Transparencies: (Currently refraction is NOT working.  Don't use).
--"Glass"
Misc.:
--"Mirror"

Leave the footer in.

-----------

Using .obj files:

Using a 3D editor, such as Blender, make a 3D model, and save it as a .obj file.  In Blender, only Edges, Material Groups, and Triangulate should be selected on exporting.  For Blender's default cube, it should look like:

# Blender3D v246 OBJ File: 
# www.blender3d.org
v 1.000000 1.000000 -1.000000
v 1.000000 -1.000000 -1.000000
v -1.000000 -1.000000 -1.000000
v -1.000000 1.000000 -1.000000
v 1.000000 0.999999 1.000000
v 0.999999 -1.000001 1.000000
v -1.000000 -1.000000 1.000000
v -1.000000 1.000000 1.000000
g Cube_Cube_Material
usemtl Material
s 1
f 5 1 4
f 5 4 8
f 3 7 8
f 3 8 4
f 2 6 3
f 6 7 3
f 1 5 2
f 5 6 2
f 5 8 6
f 8 7 6
f 1 2 3
f 1 3 4

Unfortuantely, some slight modifications are in order.  I simply remove the commented lines.  Then I get rid of "g Cube_Cube_Material" and "s 1" and change the material to "Gold" (Plastics do NOT work in .obj files).  This results in:

v 1.000000 1.000000 -1.000000
v 1.000000 -1.000000 -1.000000
v -1.000000 -1.000000 -1.000000
v -1.000000 1.000000 -1.000000
v 1.000000 0.999999 1.000000
v 0.999999 -1.000001 1.000000
v -1.000000 -1.000000 1.000000
v -1.000000 1.000000 1.000000
usemtl Gold
f 5 1 4
f 5 4 8
f 3 7 8
f 3 8 4
f 2 6 3
f 6 7 3
f 1 5 2
f 5 6 2
f 5 8 6
f 8 7 6
f 1 2 3
f 1 3 4

The .obj file can now be used by my program!