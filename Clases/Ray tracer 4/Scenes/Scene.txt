#Use '#' to add comments.  Follow
#the formats apparent here.  Just
#don't leave space(s) after any
#line containing any data.

SIZE (256,128)
#SIZE (1024,512)
BACK 200
BGCL (0,0,0)

LIGHTING True
SHADOWS False
REFLECTION True
REFRACTION False

LIGHT (128,100,-100)

#BeginOBJ
#SPHERE (108,64,0),20,Red Plastic
#SPHERE (148,64,0),20,Gold
#POLYGON ((0,0,200),(0,128,200),(256,128,200)),Orange Plastic
#EndOBJ

BeginOBJ
SPHERE (128,64,0),20,Red Plastic
EndOBJ

trans (128,64,0)

trans (0,60,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (60,0,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (-120,0,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (0,-60,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (0,-60,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (60,0,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (60,0,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

trans (0,60,0)

trans (0,0,-60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,60)
cube.obj
trans (0,0,-60)

#Leave this footer in.